Projet réalisé par pure passion, afin de découvrir les bases de la robotique. Le choix de la plateforme a été fait en particulier car elle limitait les manipulations électroniques tout en faciltant grandement l'action du code sur le matériel grâce à une API très simple d'utilisation.

Plus d'infos :

  * http://influence-pc.fr/15-07-2012-construire-un-robot-arduino-base-sur-le-chassis-dfrobot-turtle-2wd
  * http://influence-pc.fr/04-03-2013-discovery-sait-a-present-parler-video-et-code-source
